import { createRouter, createWebHistory } from 'vue-router'
import RootView from '../App.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'root',
      redirect: '/home',
      component: RootView,
      children:[
        
      ]
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('../views/ContentView.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/task',
      name: 'Task',
      component: () => import('../views/TaskManager.vue'),
      meta: { requiresAuth: true },
      redirect: '/task/release',
      children:[
        {
          path: '/task/release',
          name: 'TaskRelease',
          component: () => import('../views/task/TaskRelease.vue'),
          meta: { requiresAuth: true },
        },
        {
          path: '/task/mine',
          name: 'TaskMine',
          component: () => import('../views/task/TaskMine.vue'),
          meta: { requiresAuth: true },
        },
        {
          path: '/task/report',
          name: 'TaskReport',
          component: () => import('../views/task/TaskReport.vue'),
          meta: { requiresAuth: true },
        },
        {
          path: '/data/mine',
          name: 'DataMine',
          component: () => import('../views/data/DataMine.vue'),
          meta: { requiresAuth: true },
        },
        {
          path: '/data/platform',
          name: 'DataPlatform',
          component: () => import('../views/data/DataPlatform.vue'),
          meta: { requiresAuth: true },
        },
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !localStorage.getItem('isAuthenticated')) {
    next('/login');
  } else {
    next();
  }
});

export default router
